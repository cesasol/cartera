# Cartera
A multiplatform app to handle expenses.

I have a bad relationship with current expenses handling software, they always lack on one of the following:
- Syncs to a personal server (not a single one does this).
- Has multiple accounts support like my wallet and many bank accounts.
- Data visualization
- Expense categories and tags
- Different types of income
- Relation with other expenses/income like a personal lease
- Multiplatform (most of them are just apps)
- **Free software**

I also wanted to learn rust so I tought that this was a great way to digg into it.

**Disclaimer:** This is for personal use and I might never take into account any feature/bug that does not affect me.