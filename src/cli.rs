use crate::models::*;
use crate::CliModeCommands;
use rusty_money::{iso, Money};
use comfy_table::presets::UTF8_FULL;
use comfy_table::{Attribute, Cell, Color, ContentArrangement, Table};

pub fn init(action: CliModeCommands) {
    println!("Abriendo tu cartera");
    match action {
        CliModeCommands::List => {
            query_accounts();
        }
        CliModeCommands::AddAccount { name } => {
            Account::from_name(name.as_str())
                .save()
                .expect("not saved");
            query_accounts();
        }
        CliModeCommands::RemoveAccount { name } => {
            todo!("Handle: {}", name)
        }
        CliModeCommands::AddExpense { name } => {
            todo!("Handle: {}", name)
        }
    }
}

pub fn query_accounts() {
    let mut table = Table::new();
    table
        .load_preset(UTF8_FULL)
        .set_content_arrangement(ContentArrangement::Dynamic)
        .set_table_width(80)
        .set_header(vec![
            Cell::new("Nombre").add_attribute(Attribute::Bold),
            Cell::new("Balance").fg(Color::Green),
        ]);
        
    let mut handler = |account: Account| {
        table.add_row(vec![
            Cell::new(account.name).add_attribute(Attribute::Bold),
            Cell::new(Money::from_minor(account.balance, iso::find(&account.currency).unwrap())),]
        );
    };
    if let Err(error) = Account::iter(&mut handler) {
        println!("Error getting accounts: {}", error);
    } else {
        println!("{}", table);
    }
}
