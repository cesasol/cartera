use serde::{Deserialize, Serialize};

use super::*;

static DEFAULT_CURRENCY: &str = "MXN";

#[derive(Serialize, Deserialize, Default)]
pub(crate) struct Account<'m> {
    pub name: &'m str,
    pub balance: i64,
    pub currency: &'m str,
}

impl SledModel for Account<'_> {}
impl HasTreeKey for Account<'_> {
    const TREE_KEY: &'static str = "accounts";
}

impl<'m> Account<'m> {
    pub(crate) fn from_name(name: &'m str) -> Self { Self { name, currency: DEFAULT_CURRENCY, ..Default::default() } }

    pub(crate) fn iter_closure_mut(handler: &mut dyn for<'r> FnMut(account::Account<'r>)) -> std::result::Result<(), sled::Error> {
        for item in Account::tree()?.iter() {
            match item {
                Ok((key, value)) => {
                    println!("Decoding account with key {:?}", key.to_vec());
                    match bincode::deserialize::<Account>(&value[..]) {
                        Ok(account) => {
                            (handler)(account);
                        },
                        Err(error) => {
                            println!("Error decoding item: {}", error);
                            continue;
                        },
                    };
                
                }
                Err(error) => {
                    println!("Error: {}", error);
                    continue;
                }
            };
        }
        return Ok(());
    }
}
