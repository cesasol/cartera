#[macro_use]
extern crate lazy_static;
pub mod cli;
mod database;
mod models;
use crate::database::DB;
use cli::init as cli_init;
use human_panic::setup_panic;
use structopt::StructOpt;

#[derive(Debug, StructOpt, Default)]
pub struct CliMode {
    #[structopt(subcommand)]
    subcommand: CliModeCommands,
}
#[derive(Debug, StructOpt)]
pub enum CliModeCommands {
    List,
    AddAccount { name: String },
    RemoveAccount { name: String },
    AddExpense { name: String },
}

#[derive(Debug, StructOpt)]
enum RunMode {
    Cli(CliMode),
    Gui,
    Tui,
    Web,
}

impl Default for RunMode {
    fn default() -> Self {
        RunMode::Cli(CliMode::default())
    }
}

impl Default for CliModeCommands {
    fn default() -> Self {
        CliModeCommands::List
    }
}

#[derive(Debug, StructOpt)]
#[structopt(
    name = "Cartera",
    about = "An example of StructOpt usage.",
    version = "0.0.1"
)]
pub struct Opt {
    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,
    #[structopt(subcommand)]
    mode: Option<RunMode>,
}

fn main() {
    setup_panic!();
    let opt = Opt::from_args();
    match opt.mode {
        Some(RunMode::Cli(mode)) => cli_init(mode.subcommand),
        Some(RunMode::Gui) | Some(RunMode::Tui) | Some(RunMode::Web) => {
            todo!("Mode not implemented")
        }
        None => cli_init(CliModeCommands::default()),
    }
    DB.flush().expect("Failed flushing the database");
}
