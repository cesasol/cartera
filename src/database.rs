use directories::ProjectDirs;
use std::{fs::create_dir_all, path::Path};

lazy_static! {
    pub static ref DB: sled::Db = init();
}

fn get_db_path() -> String {
    match ProjectDirs::from("com", "cesasol", "Cartera") {
        Some(proj_dirs) => {
            let base_dir = proj_dirs.data_local_dir();
            if base_dir.exists() {
                path_to_string(base_dir)
            } else {
                match create_dir_all(base_dir) {
                    Ok(_) => path_to_string(base_dir),
                    Err(err) => panic!("Error creating data dir: {}", err),
                }
            }
        }
        None => panic!("We need a data dir"),
    }
}
fn init() -> sled::Db {
    match sled::open(get_db_path()) {
        Ok(db) => db,
        Err(err) => {
            println!("Error: {}", err);
            panic!("Error opening the database");
        }
    }
}

fn path_to_string(path: &Path) -> String {
    path.to_str().unwrap().to_string()
}
