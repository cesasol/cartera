mod account;

pub(crate) use account::*;
use crate::DB;
use serde::{Deserialize, Serialize};

pub trait SledModel {
    fn tree() -> Result<sled::Tree, sled::Error> where Self: Sized + HasTreeKey {
        DB.open_tree(Self::TREE_KEY)
    }

    fn save(&self) -> Result<&Self, sled::Error> where Self: Sized + Serialize + HasTreeKey {
        let key = DB.generate_id()?;
        let serialized: Vec<u8> = bincode::serialize(&self).unwrap();
        Self::tree()?
            .insert(key.to_be_bytes(), serialized)?;
        Ok(&self)
    }


    fn iter(handler: &mut dyn FnMut(Self)) -> Result<(), sled::Error> where Self: HasTreeKey, for<'a> Self: Deserialize<'a> {
        Self::tree()?.iter().for_each(|item| {
            match item {
                Ok((key, value)) => {
                    println!("Decoding account with key {:?}", key.to_vec());
                    match bincode::deserialize::<Self>(&value[..]) {
                        Ok(account) => {
                            (handler)(account);
                        },
                        Err(error) => {
                            println!("Error decoding item: {}", error);
                        },
                    };
                
                }
                Err(error) => {
                    println!("Error: {}", error);
                }
            };
        });
        return Ok(());
    }
}

pub trait HasTreeKey {
    const TREE_KEY: &'static str;
}